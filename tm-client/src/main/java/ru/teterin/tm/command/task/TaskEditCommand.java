package ru.teterin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Session;
import ru.teterin.tm.api.endpoint.Status;
import ru.teterin.tm.api.endpoint.Task;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

import javax.xml.datatype.XMLGregorianCalendar;

public final class TaskEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Choose task by id and edit it.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.TASK_EDIT);
        @Nullable final Session session = stateService.getSession();
        if (session == null) {
            throw new IllegalArgumentException(Constant.INCORRECT_COMMAND);
        }
        terminalService.print(Constant.ENTER_TASK_ID);
        @Nullable final String id = terminalService.readString();
        taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final Task task = taskEndpoint.findOneTask(session, id);
        terminalService.print(task);
        @NotNull final Task result = terminalService.readTask();
        @NotNull final XMLGregorianCalendar dateCreate = task.getDateCreate();
        result.setDateCreate(dateCreate);
        terminalService.print(Constant.ENTER_STATUS);
        @Nullable final String strStatus = terminalService.readString();
        if (Constant.READY.equals(strStatus)) {
            result.setStatus(Status.READY);
        }
        if (Constant.IN_PROGRESS.equals(strStatus)) {
            result.setStatus(Status.IN_PROGRESS);
        }
        if (Constant.PLANNED.equals(strStatus)) {
            result.setStatus(Status.PLANNED);
        }
        @Nullable final String projectId = task.getProjectId();
        result.setProjectId(projectId);
        @NotNull final String userId = session.getUserId();
        result.setUserId(userId);
        taskEndpoint.mergeTask(session, result);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
