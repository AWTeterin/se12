package ru.teterin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Role;
import ru.teterin.tm.api.endpoint.Session;
import ru.teterin.tm.api.endpoint.User;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

public final class UserShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-show";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show user data.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.USER_SHOW);
        @Nullable final Session session = stateService.getSession();
        if (session == null) {
            throw new IllegalArgumentException(Constant.INCORRECT_COMMAND);
        }
        @NotNull final String userId = session.getUserId();
        userEndpoint = serviceLocator.getUserEndpoint();
        @NotNull final User user = userEndpoint.findOneUser(session, userId);
        @Nullable final Role role = user.getRole();
        @NotNull final String loginInfo = Constant.USER + user.getLogin();
        terminalService.print(loginInfo);
        if (role == null) {
            throw new IllegalArgumentException(Constant.NO_ROLE);
        }
        @NotNull String roleInfo = Constant.ROLE + role.name();
        terminalService.print(roleInfo);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
