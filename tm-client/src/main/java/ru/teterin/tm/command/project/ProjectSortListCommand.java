package ru.teterin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Project;
import ru.teterin.tm.api.endpoint.Session;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

import java.util.Collection;

public final class ProjectSortListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-sort";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Find all projects and sort by field and type sort. Show result.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.PROJECT_SORT);
        @Nullable final Session session = stateService.getSession();
        if (session == null) {
            throw new IllegalArgumentException(Constant.INCORRECT_COMMAND);
        }
        terminalService.print(Constant.ENTER_SORT_FIELD);
        @Nullable final String sortOption = terminalService.readString();
        projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final Collection<Project> projects = projectEndpoint.sortAllProject(session, sortOption);
        terminalService.printCollection(projects);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
