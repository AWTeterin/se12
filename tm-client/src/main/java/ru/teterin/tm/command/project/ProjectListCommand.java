package ru.teterin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Project;
import ru.teterin.tm.api.endpoint.Session;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

import java.util.Collection;

public final class ProjectListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all project.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.PROJECT_LIST);
        @Nullable final Session session = stateService.getSession();
        if (session == null) {
            throw new IllegalArgumentException(Constant.INCORRECT_COMMAND);
        }
        projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final Collection<Project> projects = projectEndpoint.findAllProject(session);
        terminalService.printCollection(projects);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
