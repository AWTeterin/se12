package ru.teterin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Session;
import ru.teterin.tm.api.endpoint.Task;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

import java.util.Collection;

public final class TaskSearchCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-search";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Search for a string in the project name or description.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.TASK_SEARCH);
        @Nullable final Session session = stateService.getSession();
        if (session == null) {
            throw new IllegalArgumentException(Constant.INCORRECT_COMMAND);
        }
        terminalService.print(Constant.ENTER_SEARCH_STRING);
        @Nullable final String searchString = terminalService.readString();
        taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final Collection<Task> tasks = taskEndpoint.searchTaskByString(session, searchString);
        terminalService.printCollection(tasks);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
