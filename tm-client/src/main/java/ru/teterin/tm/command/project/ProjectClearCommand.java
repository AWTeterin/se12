package ru.teterin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Session;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

public final class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all project.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.PROJECT_CLEAR);
        @Nullable final Session session = stateService.getSession();
        if (session == null) {
            throw new IllegalArgumentException(Constant.INCORRECT_COMMAND);
        }
        projectEndpoint = serviceLocator.getProjectEndpoint();
        projectEndpoint.removeAllProjects(session);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
