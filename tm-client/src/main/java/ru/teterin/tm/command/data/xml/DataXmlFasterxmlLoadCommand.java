package ru.teterin.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Role;
import ru.teterin.tm.api.endpoint.Session;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

public final class DataXmlFasterxmlLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-xml-fx-load";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "The load of the subject area in the transport format XML with the use FasterXML.";
    }

    @Override
    public void execute() throws Exception {
        terminalService.print(Constant.DATA_XML_FX_LOAD);
        @Nullable final Session session = stateService.getSession();
        if (session == null) {
            throw new IllegalArgumentException(Constant.INCORRECT_COMMAND);
        }
        domainEndpoint = serviceLocator.getDomainEndpoint();
        domainEndpoint.loadXmlFasterxml(session);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public Role[] roles() {
        @NotNull final Role[] onlyAdmin = new Role[]{Role.ADMIN};
        return onlyAdmin;
    }

}
