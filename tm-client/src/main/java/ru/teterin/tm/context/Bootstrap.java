package ru.teterin.tm.context;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.endpoint.*;
import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.api.service.ITerminalService;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.endpoint.*;
import ru.teterin.tm.service.StateService;
import ru.teterin.tm.service.TerminalService;

import java.lang.Exception;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ITerminalService terminalService = new TerminalService();

    @NotNull
    private final IStateService stateService = new StateService();

    @Getter
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @Getter
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @Getter
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @Getter
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @Getter
    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpointService().getDomainEndpointPort();

    public void init() {
        initCommands();
        terminalService.print(Constant.START_MESSAGE);
        while (true) {
            @Nullable final String cmd = terminalService.readString();
            try {
                @NotNull final AbstractCommand command = stateService.getCommand(cmd);
                @Nullable final Role[] roles = command.roles();
                if (command.secure() && deniedAccess(roles)) {
                    terminalService.print(Constant.INCORRECT_COMMAND);
                    continue;
                }
                command.execute();
            } catch (@NotNull final Exception e) {
                terminalService.print(e.getMessage());
            }
        }
    }

    private void initCommands() {
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
            new Reflections(Constant.ROOT_PACKAGE).getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class clazz : classes) {
            try {
                @Nullable final Object object = clazz.newInstance();
                if (object instanceof AbstractCommand) {
                    @NotNull final AbstractCommand command = (AbstractCommand) object;
                    command.setServiceLocator(this);
                    command.setTerminalService(terminalService);
                    command.setStateService(stateService);
                    stateService.registryCommand(command);
                }
            } catch (InstantiationException | IllegalAccessException e) {
                throw new IllegalArgumentException(Constant.INVALID_COMMAND);
            }
        }
    }

    private boolean deniedAccess(
        @NotNull final Role[] roles
    ) {
        @Nullable final Session session = stateService.getSession();
        if (session == null) {
            return true;
        }
        @Nullable final Role userRole = session.getRole();
        for (@NotNull final Role role : roles) {
            if (userRole == role) {
                return false;
            }
        }
        return true;
    }

}
