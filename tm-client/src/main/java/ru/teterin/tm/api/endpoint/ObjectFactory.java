package ru.teterin.tm.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the ru.teterin.tm.api.endpoint package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "Exception");
    private final static QName _IOException_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "IOException");
    private final static QName _LoadBinary_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "loadBinary");
    private final static QName _LoadBinaryResponse_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "loadBinaryResponse");
    private final static QName _LoadJsonFasterxml_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "loadJsonFasterxml");
    private final static QName _LoadJsonFasterxmlResponse_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "loadJsonFasterxmlResponse");
    private final static QName _LoadJsonJaxb_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "loadJsonJaxb");
    private final static QName _LoadJsonJaxbResponse_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "loadJsonJaxbResponse");
    private final static QName _LoadXmlFasterxml_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "loadXmlFasterxml");
    private final static QName _LoadXmlFasterxmlResponse_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "loadXmlFasterxmlResponse");
    private final static QName _LoadXmlJaxb_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "loadXmlJaxb");
    private final static QName _LoadXmlJaxbResponse_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "loadXmlJaxbResponse");
    private final static QName _SaveBinary_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "saveBinary");
    private final static QName _SaveBinaryResponse_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "saveBinaryResponse");
    private final static QName _SaveJsonFasterxml_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "saveJsonFasterxml");
    private final static QName _SaveJsonFasterxmlResponse_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "saveJsonFasterxmlResponse");
    private final static QName _SaveJsonJaxb_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "saveJsonJaxb");
    private final static QName _SaveJsonJaxbResponse_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "saveJsonJaxbResponse");
    private final static QName _SaveXmlFasterxml_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "saveXmlFasterxml");
    private final static QName _SaveXmlFasterxmlResponse_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "saveXmlFasterxmlResponse");
    private final static QName _SaveXmlJaxb_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "saveXmlJaxb");
    private final static QName _SaveXmlJaxbResponse_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "saveXmlJaxbResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.teterin.tm.api.endpoint
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link IOException }
     */
    public IOException createIOException() {
        return new IOException();
    }

    /**
     * Create an instance of {@link LoadBinary }
     */
    public LoadBinary createLoadBinary() {
        return new LoadBinary();
    }

    /**
     * Create an instance of {@link LoadBinaryResponse }
     */
    public LoadBinaryResponse createLoadBinaryResponse() {
        return new LoadBinaryResponse();
    }

    /**
     * Create an instance of {@link LoadJsonFasterxml }
     */
    public LoadJsonFasterxml createLoadJsonFasterxml() {
        return new LoadJsonFasterxml();
    }

    /**
     * Create an instance of {@link LoadJsonFasterxmlResponse }
     */
    public LoadJsonFasterxmlResponse createLoadJsonFasterxmlResponse() {
        return new LoadJsonFasterxmlResponse();
    }

    /**
     * Create an instance of {@link LoadJsonJaxb }
     */
    public LoadJsonJaxb createLoadJsonJaxb() {
        return new LoadJsonJaxb();
    }

    /**
     * Create an instance of {@link LoadJsonJaxbResponse }
     */
    public LoadJsonJaxbResponse createLoadJsonJaxbResponse() {
        return new LoadJsonJaxbResponse();
    }

    /**
     * Create an instance of {@link LoadXmlFasterxml }
     */
    public LoadXmlFasterxml createLoadXmlFasterxml() {
        return new LoadXmlFasterxml();
    }

    /**
     * Create an instance of {@link LoadXmlFasterxmlResponse }
     */
    public LoadXmlFasterxmlResponse createLoadXmlFasterxmlResponse() {
        return new LoadXmlFasterxmlResponse();
    }

    /**
     * Create an instance of {@link LoadXmlJaxb }
     */
    public LoadXmlJaxb createLoadXmlJaxb() {
        return new LoadXmlJaxb();
    }

    /**
     * Create an instance of {@link LoadXmlJaxbResponse }
     */
    public LoadXmlJaxbResponse createLoadXmlJaxbResponse() {
        return new LoadXmlJaxbResponse();
    }

    /**
     * Create an instance of {@link SaveBinary }
     */
    public SaveBinary createSaveBinary() {
        return new SaveBinary();
    }

    /**
     * Create an instance of {@link SaveBinaryResponse }
     */
    public SaveBinaryResponse createSaveBinaryResponse() {
        return new SaveBinaryResponse();
    }

    /**
     * Create an instance of {@link SaveJsonFasterxml }
     */
    public SaveJsonFasterxml createSaveJsonFasterxml() {
        return new SaveJsonFasterxml();
    }

    /**
     * Create an instance of {@link SaveJsonFasterxmlResponse }
     */
    public SaveJsonFasterxmlResponse createSaveJsonFasterxmlResponse() {
        return new SaveJsonFasterxmlResponse();
    }

    /**
     * Create an instance of {@link SaveJsonJaxb }
     */
    public SaveJsonJaxb createSaveJsonJaxb() {
        return new SaveJsonJaxb();
    }

    /**
     * Create an instance of {@link SaveJsonJaxbResponse }
     */
    public SaveJsonJaxbResponse createSaveJsonJaxbResponse() {
        return new SaveJsonJaxbResponse();
    }

    /**
     * Create an instance of {@link SaveXmlFasterxml }
     */
    public SaveXmlFasterxml createSaveXmlFasterxml() {
        return new SaveXmlFasterxml();
    }

    /**
     * Create an instance of {@link SaveXmlFasterxmlResponse }
     */
    public SaveXmlFasterxmlResponse createSaveXmlFasterxmlResponse() {
        return new SaveXmlFasterxmlResponse();
    }

    /**
     * Create an instance of {@link SaveXmlJaxb }
     */
    public SaveXmlJaxb createSaveXmlJaxb() {
        return new SaveXmlJaxb();
    }

    /**
     * Create an instance of {@link SaveXmlJaxbResponse }
     */
    public SaveXmlJaxbResponse createSaveXmlJaxbResponse() {
        return new SaveXmlJaxbResponse();
    }

    /**
     * Create an instance of {@link Session }
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IOException }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "IOException")
    public JAXBElement<IOException> createIOException(IOException value) {
        return new JAXBElement<IOException>(_IOException_QNAME, IOException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBinary }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "loadBinary")
    public JAXBElement<LoadBinary> createLoadBinary(LoadBinary value) {
        return new JAXBElement<LoadBinary>(_LoadBinary_QNAME, LoadBinary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBinaryResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "loadBinaryResponse")
    public JAXBElement<LoadBinaryResponse> createLoadBinaryResponse(LoadBinaryResponse value) {
        return new JAXBElement<LoadBinaryResponse>(_LoadBinaryResponse_QNAME, LoadBinaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadJsonFasterxml }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "loadJsonFasterxml")
    public JAXBElement<LoadJsonFasterxml> createLoadJsonFasterxml(LoadJsonFasterxml value) {
        return new JAXBElement<LoadJsonFasterxml>(_LoadJsonFasterxml_QNAME, LoadJsonFasterxml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadJsonFasterxmlResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "loadJsonFasterxmlResponse")
    public JAXBElement<LoadJsonFasterxmlResponse> createLoadJsonFasterxmlResponse(LoadJsonFasterxmlResponse value) {
        return new JAXBElement<LoadJsonFasterxmlResponse>(_LoadJsonFasterxmlResponse_QNAME, LoadJsonFasterxmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadJsonJaxb }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "loadJsonJaxb")
    public JAXBElement<LoadJsonJaxb> createLoadJsonJaxb(LoadJsonJaxb value) {
        return new JAXBElement<LoadJsonJaxb>(_LoadJsonJaxb_QNAME, LoadJsonJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadJsonJaxbResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "loadJsonJaxbResponse")
    public JAXBElement<LoadJsonJaxbResponse> createLoadJsonJaxbResponse(LoadJsonJaxbResponse value) {
        return new JAXBElement<LoadJsonJaxbResponse>(_LoadJsonJaxbResponse_QNAME, LoadJsonJaxbResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadXmlFasterxml }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "loadXmlFasterxml")
    public JAXBElement<LoadXmlFasterxml> createLoadXmlFasterxml(LoadXmlFasterxml value) {
        return new JAXBElement<LoadXmlFasterxml>(_LoadXmlFasterxml_QNAME, LoadXmlFasterxml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadXmlFasterxmlResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "loadXmlFasterxmlResponse")
    public JAXBElement<LoadXmlFasterxmlResponse> createLoadXmlFasterxmlResponse(LoadXmlFasterxmlResponse value) {
        return new JAXBElement<LoadXmlFasterxmlResponse>(_LoadXmlFasterxmlResponse_QNAME, LoadXmlFasterxmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadXmlJaxb }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "loadXmlJaxb")
    public JAXBElement<LoadXmlJaxb> createLoadXmlJaxb(LoadXmlJaxb value) {
        return new JAXBElement<LoadXmlJaxb>(_LoadXmlJaxb_QNAME, LoadXmlJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadXmlJaxbResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "loadXmlJaxbResponse")
    public JAXBElement<LoadXmlJaxbResponse> createLoadXmlJaxbResponse(LoadXmlJaxbResponse value) {
        return new JAXBElement<LoadXmlJaxbResponse>(_LoadXmlJaxbResponse_QNAME, LoadXmlJaxbResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBinary }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "saveBinary")
    public JAXBElement<SaveBinary> createSaveBinary(SaveBinary value) {
        return new JAXBElement<SaveBinary>(_SaveBinary_QNAME, SaveBinary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBinaryResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "saveBinaryResponse")
    public JAXBElement<SaveBinaryResponse> createSaveBinaryResponse(SaveBinaryResponse value) {
        return new JAXBElement<SaveBinaryResponse>(_SaveBinaryResponse_QNAME, SaveBinaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveJsonFasterxml }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "saveJsonFasterxml")
    public JAXBElement<SaveJsonFasterxml> createSaveJsonFasterxml(SaveJsonFasterxml value) {
        return new JAXBElement<SaveJsonFasterxml>(_SaveJsonFasterxml_QNAME, SaveJsonFasterxml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveJsonFasterxmlResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "saveJsonFasterxmlResponse")
    public JAXBElement<SaveJsonFasterxmlResponse> createSaveJsonFasterxmlResponse(SaveJsonFasterxmlResponse value) {
        return new JAXBElement<SaveJsonFasterxmlResponse>(_SaveJsonFasterxmlResponse_QNAME, SaveJsonFasterxmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveJsonJaxb }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "saveJsonJaxb")
    public JAXBElement<SaveJsonJaxb> createSaveJsonJaxb(SaveJsonJaxb value) {
        return new JAXBElement<SaveJsonJaxb>(_SaveJsonJaxb_QNAME, SaveJsonJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveJsonJaxbResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "saveJsonJaxbResponse")
    public JAXBElement<SaveJsonJaxbResponse> createSaveJsonJaxbResponse(SaveJsonJaxbResponse value) {
        return new JAXBElement<SaveJsonJaxbResponse>(_SaveJsonJaxbResponse_QNAME, SaveJsonJaxbResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveXmlFasterxml }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "saveXmlFasterxml")
    public JAXBElement<SaveXmlFasterxml> createSaveXmlFasterxml(SaveXmlFasterxml value) {
        return new JAXBElement<SaveXmlFasterxml>(_SaveXmlFasterxml_QNAME, SaveXmlFasterxml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveXmlFasterxmlResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "saveXmlFasterxmlResponse")
    public JAXBElement<SaveXmlFasterxmlResponse> createSaveXmlFasterxmlResponse(SaveXmlFasterxmlResponse value) {
        return new JAXBElement<SaveXmlFasterxmlResponse>(_SaveXmlFasterxmlResponse_QNAME, SaveXmlFasterxmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveXmlJaxb }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "saveXmlJaxb")
    public JAXBElement<SaveXmlJaxb> createSaveXmlJaxb(SaveXmlJaxb value) {
        return new JAXBElement<SaveXmlJaxb>(_SaveXmlJaxb_QNAME, SaveXmlJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveXmlJaxbResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "saveXmlJaxbResponse")
    public JAXBElement<SaveXmlJaxbResponse> createSaveXmlJaxbResponse(SaveXmlJaxbResponse value) {
        return new JAXBElement<SaveXmlJaxbResponse>(_SaveXmlJaxbResponse_QNAME, SaveXmlJaxbResponse.class, null, value);
    }

}
