package ru.teterin.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.endpoint.*;
import ru.teterin.tm.api.service.*;
import ru.teterin.tm.endpoint.*;
import ru.teterin.tm.service.*;

import javax.xml.ws.Endpoint;

@Getter
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ITaskService taskService = new TaskService(this);

    @NotNull
    private final IProjectService projectService = new ProjectService(this);

    @NotNull
    private final IUserService userService = new UserService(this);

    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ISessionService sessionService = new SessionService(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    public void init() {
        registry(sessionEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(domainEndpoint);
    }

    private void registry(
        @NotNull final Object endpoint
    ) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

}
