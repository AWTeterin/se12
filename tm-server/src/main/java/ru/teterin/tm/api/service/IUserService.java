package ru.teterin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.User;

public interface IUserService extends IService<User> {

    @Nullable
    public User findByLogin(
        @Nullable final String login
    ) throws Exception;

}
