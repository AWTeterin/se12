package ru.teterin.tm.api.context;

import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.api.service.*;

public interface IServiceLocator {

    @NotNull
    public IPropertyService getPropertyService();

    @NotNull
    public IProjectService getProjectService();

    @NotNull
    public ITaskService getTaskService();

    @NotNull
    public IUserService getUserService();

    @NotNull
    public IDomainService getDomainService();

    @NotNull
    public ISessionService getSessionService();

}
