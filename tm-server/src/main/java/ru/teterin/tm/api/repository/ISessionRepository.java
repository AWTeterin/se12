package ru.teterin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.entity.Session;

public interface ISessionRepository extends IRepository<Session> {

    public void validate(
        @NotNull final String userId,
        @NotNull final String sessionId
    ) throws Exception;

    public boolean contains(
        @NotNull final String sessionId
    ) throws Exception;

}
