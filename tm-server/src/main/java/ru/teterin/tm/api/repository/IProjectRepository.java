package ru.teterin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.Project;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @Nullable
    public Project findOne(
        @NotNull final String userId,
        @NotNull final String id
    ) throws Exception;

    @NotNull
    public List<Project> findAll(
        @NotNull final String userId
    ) throws Exception;

    public void remove(
        @NotNull final String userId,
        @NotNull final String id
    ) throws Exception;

    public void removeAll(
        @NotNull final String userId
    ) throws Exception;

    @NotNull
    public Collection<Project> searchByString(
        @NotNull final String userId,
        @NotNull final String searchString
    ) throws SQLException, Exception;

    @NotNull
    public Collection<Project> sortByDateCreate(
        @NotNull final String userId,
        @NotNull final String sortType
    ) throws Exception;

    @NotNull
    public Collection<Project> sortByDateStart(
        @NotNull final String userId,
        @NotNull final String sortType
    ) throws Exception;

    @NotNull
    public Collection<Project> sortByDateEnd(
        @NotNull final String userId,
        @NotNull final String sortType
    ) throws Exception;

    @NotNull
    public Collection<Project> sortByStatus(
        @NotNull final String userId,
        @NotNull final String sortType
    ) throws Exception;

}
