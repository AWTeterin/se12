package ru.teterin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.Domain;

import java.nio.file.Path;

public interface IDomainService {

    public void save(
        @Nullable final Domain domain
    ) throws Exception;

    public void load(
        @Nullable final Domain domain
    ) throws Exception;

    public void saveBinary() throws Exception;

    public void loadBinary() throws Exception;

    public void saveJsonFasterxml() throws Exception;

    public void saveJsonJaxb() throws Exception;

    public void loadJsonFasterxml() throws Exception;

    public void loadJsonJaxb() throws Exception;

    public void saveXmlFasterxml() throws Exception;

    public void saveXmlJaxb() throws Exception;

    public void loadXmlFasterxml() throws Exception;

    public void loadXmlJaxb() throws Exception;

    public void clearData() throws Exception;

    @NotNull
    public Domain readData(
        @NotNull final Path path
    ) throws Exception;

}
