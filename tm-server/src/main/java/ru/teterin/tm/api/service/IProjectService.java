package ru.teterin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.Project;
import ru.teterin.tm.entity.Task;

import java.util.Collection;
import java.util.List;

public interface IProjectService extends IService<Project> {

    @NotNull
    public Project findOne(
        @Nullable final String userId,
        @Nullable final String id
    ) throws Exception;

    @NotNull
    public List<Project> findAll(
        @Nullable final String userId
    ) throws Exception;

    public void persist(
        @Nullable final String userId,
        @Nullable final Project project
    ) throws Exception;

    public void merge(
        @Nullable final String userId,
        @Nullable final Project project
    ) throws Exception;

    public void remove(
        @Nullable final String userId,
        @Nullable final String id
    ) throws Exception;

    public void removeAll(
        @Nullable final String userId
    ) throws Exception;

    @NotNull
    public Collection<Task> findAllTaskByProjectId(
        @Nullable final String userId,
        @Nullable final String id
    ) throws Exception;

    @NotNull
    public Collection<Project> findAndSortAll(
        @Nullable final String userId,
        @Nullable String sortOption
    ) throws Exception;

    @NotNull
    public Collection<Project> searchByString(
        @Nullable final String userId,
        @Nullable String searchString
    ) throws Exception;

}
