package ru.teterin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService extends IService<Task> {

    @Nullable
    public Task findOne(
        @NotNull final String userId,
        @NotNull final String id
    ) throws Exception;

    @NotNull
    public List<Task> findAll(
        @NotNull final String userId
    ) throws Exception;

    public void persist(
        @NotNull final String userId,
        @NotNull final Task task
    ) throws Exception;

    public void merge(
        @NotNull final String userId,
        @NotNull final Task task
    ) throws Exception;

    public void remove(
        @NotNull final String userId,
        @NotNull final String id
    ) throws Exception;

    public void removeAll(
        @NotNull final String userId
    ) throws Exception;

    @NotNull
    public Task linkTask(
        @Nullable final String userId,
        @Nullable final String projectId,
        @Nullable final String id
    ) throws Exception;

    @NotNull
    public Collection<Task> findAndSortAll(
        @Nullable final String userId,
        @Nullable String sortOption
    ) throws Exception;

    @NotNull
    public Collection<Task> searchByString(
        @Nullable final String userId,
        @Nullable String searchString
    ) throws Exception;

}
