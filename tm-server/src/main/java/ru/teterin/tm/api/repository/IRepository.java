package ru.teterin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.AbstractEntity;

import java.util.List;

public interface IRepository<T extends AbstractEntity> {

    @Nullable
    public T findOne(
        @NotNull final String id
    ) throws Exception;

    @NotNull
    public List<T> findAll() throws Exception;

    public void persist(
        @NotNull final T t
    ) throws Exception;

    public void merge(
        @NotNull final T t
    ) throws Exception;

    public void remove(
        @NotNull final String id
    ) throws Exception;

    public void removeAll() throws Exception;

}
