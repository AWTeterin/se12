package ru.teterin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.Session;
import ru.teterin.tm.enumerated.Role;

public interface ISessionService extends IService<Session> {

    @NotNull
    public Session open(
        @Nullable final String login,
        @Nullable final String password
    ) throws Exception;

    public void validate(
        @Nullable final Session session
    ) throws Exception;

    public void validateWithRole(
        @Nullable final Session session,
        @Nullable final Role role
    ) throws Exception;

    public void close(
        @Nullable final Session session
    ) throws Exception;

    @Nullable
    public Session sign(
        @Nullable final Session session
    );

}
