package ru.teterin.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint {

    @WebMethod
    public void saveBinary(
        @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    @WebMethod
    public void loadBinary(
        @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    @WebMethod
    public void saveJsonFasterxml(
        @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    @WebMethod
    public void saveJsonJaxb(
        @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    @WebMethod
    public void loadJsonFasterxml(
        @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    @WebMethod
    public void loadJsonJaxb(
        @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    @WebMethod
    public void saveXmlFasterxml(
        @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    @WebMethod
    public void saveXmlJaxb(
        @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    @WebMethod
    public void loadXmlFasterxml(
        @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    @WebMethod
    public void loadXmlJaxb(
        @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

}
