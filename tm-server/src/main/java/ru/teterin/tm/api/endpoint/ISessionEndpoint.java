package ru.teterin.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {

    @Nullable
    @WebMethod
    public Session openSession(
        @WebParam(name = "login") @Nullable final String login,
        @WebParam(name = "password") @Nullable final String password
    ) throws Exception;

    @Nullable
    @WebMethod
    public Session closeSession(
        @WebParam(name = "session") @Nullable final Session session
    ) throws Exception;

}
