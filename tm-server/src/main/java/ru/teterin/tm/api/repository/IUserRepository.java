package ru.teterin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    public User findByLogin(
        @NotNull final String login
    ) throws Exception;

}
