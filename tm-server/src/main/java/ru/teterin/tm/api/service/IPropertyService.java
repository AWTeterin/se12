package ru.teterin.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    public String getServerHost();

    @NotNull
    public String getServerPort();

    @NotNull
    public String getSessionSalt();

    @NotNull
    public Integer getSessionCycle();

    @NotNull
    public String getSqlDriver();

    @NotNull
    public String getSqlUrl();

    @NotNull
    public String getSqlLogin();

    @NotNull
    public String getSqlPassword();

}
