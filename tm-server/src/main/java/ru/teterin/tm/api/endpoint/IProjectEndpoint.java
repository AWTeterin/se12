package ru.teterin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.Project;
import ru.teterin.tm.entity.Session;
import ru.teterin.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @WebMethod
    public void mergeProject(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "project") @Nullable final Project project
    ) throws Exception;

    @WebMethod
    public void persistProject(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "project") @Nullable final Project project
    ) throws Exception;

    @NotNull
    @WebMethod
    public Project findOneProject(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception;

    @NotNull
    @WebMethod
    public List<Project> findAllProject(
        @WebParam(name = "session") @Nullable final Session session
    ) throws Exception;

    @WebMethod
    public void removeProject(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception;

    @WebMethod
    public void removeAllProjects(
        @WebParam(name = "session") @Nullable final Session session
    ) throws Exception;

    @NotNull
    @WebMethod
    public Collection<Project> searchProjectByString(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "string") @Nullable final String string
    ) throws Exception;

    @NotNull
    @WebMethod
    public Collection<Project> sortAllProject(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "sortOption") @Nullable final String sortOption
    ) throws Exception;

    @NotNull
    @WebMethod
    public Collection<Task> findAllProjectTasks(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception;

}
