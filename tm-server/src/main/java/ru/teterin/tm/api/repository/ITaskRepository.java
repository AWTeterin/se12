package ru.teterin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @Nullable
    public Task findOne(
        @NotNull final String userId,
        @NotNull final String id
    ) throws Exception;

    @NotNull
    public List<Task> findAll(
        @NotNull final String userId
    ) throws Exception;

    public void remove(
        @NotNull final String userId,
        @NotNull final String id
    ) throws Exception;

    public void removeAll(
        @NotNull final String userId
    ) throws Exception;

    @NotNull
    public Collection<Task> findAllByProjectId(
        @NotNull final String userId,
        @NotNull final String projectId
    ) throws Exception;

    @NotNull
    public Collection<Task> searchByString(
        @NotNull final String userId,
        @NotNull final String searchString
    ) throws Exception;

    @NotNull
    public Collection<Task> sortByDateCreate(
        @NotNull final String userId,
        @NotNull final String sortType
    ) throws Exception;

    @NotNull
    public Collection<Task> sortByDateStart(
        @NotNull final String userId,
        @NotNull final String sortType
    ) throws Exception;

    @NotNull
    public Collection<Task> sortByDateEnd(
        @NotNull final String userId,
        @NotNull final String sortType
    ) throws Exception;

    @NotNull
    public Collection<Task> sortByStatus(
        @NotNull final String userId,
        @NotNull final String sortType
    ) throws Exception;

}
