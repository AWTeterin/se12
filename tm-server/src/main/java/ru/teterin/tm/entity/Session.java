package ru.teterin.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Session extends AbstractEntity {

    @Nullable
    private Role role;

    private long timestamp = System.currentTimeMillis();

    @Nullable
    private String signature;

    @NotNull
    public Session clone() {
        @NotNull Session session = new Session();
        session.setId(id);
        session.setUserId(userId);
        session.setRole(role);
        session.setTimestamp(timestamp);
        session.setSignature(signature);
        return session;
    }

}