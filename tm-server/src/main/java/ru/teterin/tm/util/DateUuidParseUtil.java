package ru.teterin.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.constant.ExceptionConstant;

import java.text.ParseException;
import java.util.Date;
import java.util.UUID;

public final class DateUuidParseUtil {

    @NotNull
    public static String isUuid(
        @NotNull final String id
    ) {
        try {
            UUID.fromString(id);
            return id;
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(ExceptionConstant.INCORRECT_ID);
        }
    }

    @NotNull
    public static Date isDate(
        @NotNull final String stringDate
    ) {
        try {
            @NotNull final Date date = Constant.DATE_FORMAT.parse(stringDate);
            return date;
        } catch (ParseException e) {
            throw new IllegalArgumentException(ExceptionConstant.INCORRECT_DATE);
        }
    }

    @Nullable
    public static java.sql.Date toSqlDate(
        @Nullable final Date date
    ) {
        if (date == null) return null;
        @NotNull final java.sql.Date result = new java.sql.Date(date.getTime());
        return result;
    }

}
