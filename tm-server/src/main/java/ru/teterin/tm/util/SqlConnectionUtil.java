package ru.teterin.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.service.PropertyService;

import java.sql.Connection;
import java.sql.DriverManager;

public final class SqlConnectionUtil {

    @NotNull
    private static final PropertyService propertyService = new PropertyService();

    @NotNull
    private static final String driver = propertyService.getSqlDriver();

    @NotNull
    private static final String url = propertyService.getSqlUrl();

    @NotNull
    private static final String login = propertyService.getSqlLogin();

    @NotNull
    private static final String password = propertyService.getSqlPassword();

    public static Connection connect() throws Exception {
        Class.forName(driver);
        @NotNull final Connection connection = DriverManager.getConnection(url, login, password);
        connection.setAutoCommit(false);
        return connection;
    }

}
