package ru.teterin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.repository.IProjectRepository;
import ru.teterin.tm.api.repository.ITaskRepository;
import ru.teterin.tm.api.service.IProjectService;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.constant.ExceptionConstant;
import ru.teterin.tm.entity.Project;
import ru.teterin.tm.entity.Task;
import ru.teterin.tm.exception.AccessForbiddenException;
import ru.teterin.tm.exception.ObjectExistException;
import ru.teterin.tm.exception.ObjectNotFoundException;
import ru.teterin.tm.repository.ProjectRepository;
import ru.teterin.tm.repository.TaskRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    public ProjectService(
        @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public Project findOne(
        @Nullable final String id
    ) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
        @Nullable final Project project = projectRepository.findOne(id);
        if (project == null) {
            connection.close();
            throw new ObjectNotFoundException();
        }
        connection.close();
        return project;
    }

    @NotNull
    @Override
    public Project findOne(
        @Nullable final String userId,
        @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
        @Nullable final Project project = projectRepository.findOne(userId, id);
        if (project == null) {
            connection.close();
            throw new ObjectNotFoundException();
        }
        connection.close();
        return project;
    }

    @NotNull
    @Override
    public List<Project> findAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
        @Nullable final List<Project> projects = projectRepository.findAll();
        connection.close();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findAll(
        @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
        @Nullable final List<Project> projects = projectRepository.findAll(userId);
        connection.close();
        return projects;
    }

    @Override
    public void persist(
        @Nullable final Project project
    ) throws Exception {
        if (project == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String name = project.getName();
        if (name == null || name.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
        try {
            projectRepository.persist(project);
        } catch (SQLException e) {
            connection.rollback();
            throw new ObjectExistException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void persist(
        @Nullable final String userId,
        @Nullable final Project project
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (project == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String name = project.getName();
        if (name == null || name.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String projectUserId = project.getUserId();
        if (!userId.equals(projectUserId)) {
            throw new AccessForbiddenException();
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
        try {
            projectRepository.persist(project);
        } catch (SQLException e) {
            connection.rollback();
            throw new ObjectExistException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void merge(
        @Nullable final Project project
    ) throws Exception {
        if (project == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String name = project.getName();
        if (name == null || name.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
        try {
            projectRepository.merge(project);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_UPDATE);
        } finally {
            connection.close();
        }
    }

    @Override
    public void merge(
        @Nullable final String userId,
        @Nullable final Project project
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (project == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String name = project.getName();
        if (name == null || name.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String projectUserId = project.getUserId();
        if (!userId.equals(projectUserId)) {
            throw new AccessForbiddenException();
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
        try {
            projectRepository.merge(project);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_UPDATE);
        } finally {
            connection.close();
        }
    }

    @Override
    public void remove(
        @Nullable final String id
    ) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
        try {
            projectRepository.remove(id);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            connection.close();
        }
    }

    @Override
    public void remove(
        @Nullable final String userId,
        @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
        try {
            projectRepository.remove(userId, id);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
        try {
            projectRepository.removeAll();
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll(
        @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
        try {
            projectRepository.removeAll(userId);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public Collection<Task> findAllTaskByProjectId(
        @Nullable final String userId,
        @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        findOne(userId, id);
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        @NotNull final Collection<Task> tasks = taskRepository.findAllByProjectId(userId, id);
        connection.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Project> findAndSortAll(
        @Nullable final String userId,
        @Nullable String sortOption
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (sortOption == null) {
            sortOption = Constant.CREATE_ASC_SORT;
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
        @NotNull final Collection<Project> projects;
        switch (sortOption) {
            default:
                projects = projectRepository.sortByDateCreate(userId, Constant.ASC);
                break;
            case Constant.CREATE_DESC_SORT:
                projects = projectRepository.sortByDateCreate(userId, Constant.DESC);
                break;
            case Constant.START_DATE_ASC_SORT:
                projects = projectRepository.sortByDateStart(userId, Constant.ASC);
                break;
            case Constant.START_DATE_DESC_SORT:
                projects = projectRepository.sortByDateStart(userId, Constant.DESC);
                break;
            case Constant.END_DATE_ASC_SORT:
                projects = projectRepository.sortByDateEnd(userId, Constant.ASC);
                break;
            case Constant.END_DATE_DESC_SORT:
                projects = projectRepository.sortByDateEnd(userId, Constant.DESC);
                break;
            case Constant.STATUS_ASC_SORT:
                projects = projectRepository.sortByStatus(userId, Constant.ASC);
                break;
            case Constant.STATUS_DESC_SORT:
                projects = projectRepository.sortByStatus(userId, Constant.DESC);
                break;
        }
        connection.close();
        return projects;
    }

    @NotNull
    @Override
    public Collection<Project> searchByString(
        @Nullable final String userId,
        @Nullable String searchString
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (searchString == null) {
            searchString = "";
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
        @NotNull final Collection<Project> projects = projectRepository.searchByString(userId, searchString);
        connection.close();
        return projects;
    }

}
