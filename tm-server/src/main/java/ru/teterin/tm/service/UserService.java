package ru.teterin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.repository.IUserRepository;
import ru.teterin.tm.api.service.IUserService;
import ru.teterin.tm.constant.ExceptionConstant;
import ru.teterin.tm.entity.User;
import ru.teterin.tm.exception.ObjectExistException;
import ru.teterin.tm.exception.ObjectNotFoundException;
import ru.teterin.tm.repository.UserRepository;
import ru.teterin.tm.util.PasswordHashUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {


    public UserService(
        @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public User findOne(
        @Nullable final String id
    ) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository userRepository = new UserRepository(connection);
        @Nullable final User user = userRepository.findOne(id);
        if (user == null) {
            connection.close();
            throw new ObjectNotFoundException();
        }
        connection.close();
        return user;
    }

    @NotNull
    @Override
    public List<User> findAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository userRepository = new UserRepository(connection);
        @Nullable final List<User> users = userRepository.findAll();
        connection.close();
        return users;
    }

    @Override
    public void persist(
        @Nullable final User user
    ) throws Exception {
        if (user == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String password = user.getPassword();
        if (password == null || password.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @Nullable final String hashPassword = PasswordHashUtil.md5(password);
        user.setPassword(hashPassword);
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository userRepository = new UserRepository(connection);
        try {
            userRepository.persist(user);
        } catch (SQLException e) {
            connection.rollback();
            throw new ObjectExistException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void merge(
        @Nullable final User user
    ) throws Exception {
        if (user == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @Nullable final String hashPassword = PasswordHashUtil.md5(user.getPassword());
        user.setPassword(hashPassword);
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository userRepository = new UserRepository(connection);
        try {
            userRepository.merge(user);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_UPDATE);
        } finally {
            connection.close();
        }
    }

    @Override
    public void remove(
        @Nullable final String id
    ) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository userRepository = new UserRepository(connection);
        try {
            userRepository.remove(id);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository userRepository = new UserRepository(connection);
        try {
            userRepository.removeAll();
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public User findByLogin(
        @Nullable final String login
    ) throws Exception {
        if (login == null || login.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_LOGIN);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository userRepository = new UserRepository(connection);
        @Nullable final User user = userRepository.findByLogin(login);
        connection.close();
        return user;
    }

}
