package ru.teterin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.repository.ISessionRepository;
import ru.teterin.tm.api.repository.IUserRepository;
import ru.teterin.tm.api.service.IPropertyService;
import ru.teterin.tm.api.service.ISessionService;
import ru.teterin.tm.constant.ExceptionConstant;
import ru.teterin.tm.entity.Session;
import ru.teterin.tm.entity.User;
import ru.teterin.tm.enumerated.Role;
import ru.teterin.tm.exception.AccessForbiddenException;
import ru.teterin.tm.exception.ObjectExistException;
import ru.teterin.tm.exception.ObjectNotFoundException;
import ru.teterin.tm.repository.SessionRepository;
import ru.teterin.tm.repository.UserRepository;
import ru.teterin.tm.util.PasswordHashUtil;
import ru.teterin.tm.util.SignatureUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final IPropertyService propertyService;

    public SessionService(
        @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        this.propertyService = serviceLocator.getPropertyService();
    }

    @NotNull
    @Override
    public Session findOne(
        @Nullable final String id
    ) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(connection);
        @Nullable final Session session = sessionRepository.findOne(id);
        if (session == null) {
            throw new ObjectNotFoundException();
        }
        connection.close();
        return session;
    }

    @NotNull
    @Override
    public List<Session> findAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(connection);
        @Nullable final List<Session> sessions = sessionRepository.findAll();
        connection.close();
        return sessions;
    }

    @Override
    public void persist(
        @Nullable final Session session
    ) throws Exception {
        if (session == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(connection);
        try {
            sessionRepository.persist(session);
        } catch (SQLException e) {
            connection.rollback();
            throw new ObjectExistException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void merge(
        @Nullable final Session session
    ) throws Exception {
        if (session == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(connection);
        try {
            sessionRepository.merge(session);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_UPDATE);
        } finally {
            connection.close();
        }
    }

    @Override
    public void remove(
        @Nullable final String id
    ) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(connection);
        try {
            sessionRepository.remove(id);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(connection);
        try {
            sessionRepository.removeAll();
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public Session open(
        @Nullable final String login,
        @Nullable final String password
    ) throws Exception {
        final boolean noLogin = login == null || login.isEmpty();
        if (noLogin) {
            throw new Exception(ExceptionConstant.EMPTY_LOGIN);
        }
        final boolean noPassword = password == null || password.isEmpty();
        if (noPassword) {
            throw new Exception(ExceptionConstant.EMPTY_PASSWORD);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository userRepository = new UserRepository(connection);
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) {
            throw new Exception(ExceptionConstant.OBJECT_NOT_FOUND);
        }
        @Nullable final String hashPassword = PasswordHashUtil.md5(password);
        if (hashPassword == null) {
            throw new Exception(ExceptionConstant.EMPTY_PASSWORD);
        }
        @NotNull final String userPassword = user.getPassword();
        final boolean incorrectPassword = !hashPassword.equals(userPassword);
        if (incorrectPassword) {
            throw new Exception(ExceptionConstant.INCORRECT_PASSWORD);
        }
        @NotNull final Session session = new Session();
        @NotNull final String userId = user.getId();
        session.setUserId(userId);
        @Nullable final Role userRole = user.getRole();
        session.setRole(userRole);
        sign(session);
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(connection);
        try {
            sessionRepository.persist(session);
        } catch (SQLException e) {
            connection.rollback();
            throw new ObjectExistException();
        } finally {
            connection.close();
        }
        return session;
    }

    @Override
    public void validate(
        @Nullable final Session session
    ) throws Exception {
        if (session == null) {
            throw new AccessForbiddenException();
        }
        @Nullable final String signature = session.getSignature();
        final boolean noSignature = signature == null || signature.isEmpty();
        if (noSignature) {
            throw new AccessForbiddenException();
        }
        @Nullable final String userId = session.getUserId();
        final boolean noUserId = userId == null || userId.isEmpty();
        if (noUserId) {
            throw new AccessForbiddenException();
        }
        @NotNull final Session temp = session.clone();
        if (temp == null) {
            throw new AccessForbiddenException();
        }
        @NotNull final String signatureSource = session.getSignature();
        sign(temp);
        @Nullable final String signatureTarget = temp.getSignature();
        final boolean incorrectSignature = !signatureSource.equals(signatureTarget);
        if (incorrectSignature) {
            throw new AccessForbiddenException();
        }
        @NotNull final String sessionId = session.getId();
        @NotNull final Connection connection = getConnection();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(connection);
        final boolean incorrectId = !sessionRepository.contains(sessionId);
        if (incorrectId) {
            throw new AccessForbiddenException();
        }
        connection.close();
    }

    @Override
    public void validateWithRole(
        @Nullable final Session session,
        @Nullable final Role role
    ) throws Exception {
        validate(session);
        @NotNull final Role userRole = session.getRole();
        final boolean accessDenied = !userRole.equals(role);
        if (accessDenied) {
            throw new AccessForbiddenException();
        }
    }

    @Override
    public void close(
        @Nullable final Session session
    ) throws Exception {
        validate(session);
        @NotNull final String sessionId = session.getId();
        @NotNull final Connection connection = getConnection();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(connection);
        sessionRepository.remove(sessionId);
    }

    @Nullable
    @Override
    public Session sign(
        @Nullable final Session session
    ) {
        if (session == null) {
            return null;
        }
        session.setSignature(null);
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

}
