package ru.teterin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.repository.IProjectRepository;
import ru.teterin.tm.api.repository.ITaskRepository;
import ru.teterin.tm.api.service.ITaskService;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.constant.ExceptionConstant;
import ru.teterin.tm.entity.Project;
import ru.teterin.tm.entity.Task;
import ru.teterin.tm.exception.AccessForbiddenException;
import ru.teterin.tm.exception.ObjectExistException;
import ru.teterin.tm.exception.ObjectNotFoundException;
import ru.teterin.tm.repository.ProjectRepository;
import ru.teterin.tm.repository.TaskRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    public TaskService(
        @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public Task findOne(
        @Nullable final String id
    ) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        @Nullable final Task task = taskRepository.findOne(id);
        if (task == null) {
            connection.close();
            throw new ObjectNotFoundException();
        }
        connection.close();
        return task;
    }

    @NotNull
    @Override
    public Task findOne(
        @Nullable final String userId,
        @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        @Nullable final Task task = taskRepository.findOne(userId, id);
        if (task == null) {
            connection.close();
            throw new ObjectNotFoundException();
        }
        connection.close();
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        @Nullable final List<Task> tasks = taskRepository.findAll();
        connection.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findAll(
        @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        @Nullable final List<Task> tasks = taskRepository.findAll(userId);
        connection.close();
        return tasks;
    }

    @Override
    public void persist(
        @Nullable final Task task
    ) throws Exception {
        if (task == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String name = task.getName();
        if (name == null || name.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        try {
            taskRepository.persist(task);
        } catch (SQLException e) {
            connection.rollback();
            throw new ObjectExistException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void persist(
        @Nullable final String userId,
        @Nullable final Task task
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (task == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String name = task.getName();
        if (name == null || name.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String taskUserId = task.getUserId();
        if (!userId.equals(taskUserId)) {
            throw new AccessForbiddenException();
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        try {
            taskRepository.persist(task);
        } catch (SQLException e) {
            connection.rollback();
            throw new ObjectExistException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void merge(
        @Nullable final Task task
    ) throws Exception {
        if (task == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String name = task.getName();
        if (name == null || name.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        try {
            taskRepository.merge(task);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_UPDATE);
        } finally {
            connection.close();
        }
    }

    @Override
    public void merge(
        @Nullable final String userId,
        @Nullable final Task task
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (task == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String name = task.getName();
        if (name == null || name.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String taskUserId = task.getUserId();
        if (!userId.equals(taskUserId)) {
            throw new AccessForbiddenException();
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        try {
            taskRepository.merge(task);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_UPDATE);
        } finally {
            connection.close();
        }
    }

    @Override
    public void remove(
        @Nullable final String id
    ) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final TaskRepository taskRepository = new TaskRepository(connection);
        try {
            taskRepository.remove(id);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            connection.close();
        }
    }

    @Override
    public void remove(
        @Nullable final String userId,
        @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        try {
            taskRepository.remove(userId, id);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        try {
            taskRepository.removeAll();
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll(
        @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final TaskRepository taskRepository = new TaskRepository(connection);
        try {
            taskRepository.removeAll(userId);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public Task linkTask(
        @Nullable final String userId,
        @Nullable final String projectId,
        @Nullable final String id
    ) throws Exception {
        final boolean noProjectId = projectId == null || projectId.isEmpty();
        final boolean noTaskId = id == null || id.isEmpty();
        final boolean noUserId = userId == null || userId.isEmpty();
        if (noProjectId || noTaskId || noUserId) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
        @Nullable final Project project = projectRepository.findOne(userId, projectId);
        if (project == null) {
            throw new ObjectNotFoundException();
        }
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        @Nullable final Task task = taskRepository.findOne(userId, id);
        if (task == null) {
            throw new ObjectNotFoundException();
        }
        task.setProjectId(projectId);
        connection.close();
        merge(task);
        return task;
    }

    @NotNull
    @Override
    public Collection<Task> findAndSortAll(
        @Nullable final String userId,
        @Nullable String sortOption
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (sortOption == null) {
            sortOption = Constant.CREATE_ASC_SORT;
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        @NotNull final Collection<Task> tasks;
        switch (sortOption) {
            default:
                tasks = taskRepository.sortByDateCreate(userId, Constant.ASC);
                break;
            case Constant.CREATE_DESC_SORT:
                tasks = taskRepository.sortByDateCreate(userId, Constant.DESC);
                break;
            case Constant.START_DATE_ASC_SORT:
                tasks = taskRepository.sortByDateStart(userId, Constant.ASC);
                break;
            case Constant.START_DATE_DESC_SORT:
                tasks = taskRepository.sortByDateStart(userId, Constant.DESC);
                break;
            case Constant.END_DATE_ASC_SORT:
                tasks = taskRepository.sortByDateEnd(userId, Constant.ASC);
                break;
            case Constant.END_DATE_DESC_SORT:
                tasks = taskRepository.sortByDateEnd(userId, Constant.DESC);
                break;
            case Constant.STATUS_ASC_SORT:
                tasks = taskRepository.sortByStatus(userId, Constant.ASC);
                break;
            case Constant.STATUS_DESC_SORT:
                tasks = taskRepository.sortByStatus(userId, Constant.DESC);
                break;
        }
        connection.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> searchByString(
        @Nullable final String userId,
        @Nullable String searchString
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (searchString == null) {
            searchString = "";
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        @NotNull final Collection<Task> tasks = taskRepository.searchByString(userId, searchString);
        connection.close();
        return tasks;
    }

}
