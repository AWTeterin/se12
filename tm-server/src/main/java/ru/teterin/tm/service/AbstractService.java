package ru.teterin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.service.IService;
import ru.teterin.tm.entity.AbstractEntity;
import ru.teterin.tm.util.SqlConnectionUtil;

import java.sql.Connection;
import java.util.List;

public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @NotNull
    protected final IServiceLocator serviceLocator;

    public AbstractService(
        @NotNull final IServiceLocator serviceLocator
    ) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public abstract T findOne(
        @Nullable final String id
    ) throws Exception;

    @NotNull
    @Override
    public abstract List<T> findAll() throws Exception;

    @Override
    public abstract void persist(
        @Nullable final T t
    ) throws Exception;

    @Override
    public abstract void merge(
        @Nullable final T t
    ) throws Exception;

    @Override
    public abstract void remove(
        @Nullable final String id
    ) throws Exception;

    @Override
    public abstract void removeAll() throws Exception;

    @NotNull
    protected Connection getConnection() throws Exception {
        @NotNull final Connection connection = SqlConnectionUtil.connect();
        return connection;
    }

}
