package ru.teterin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.repository.ITaskRepository;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.constant.FieldConstant;
import ru.teterin.tm.entity.Task;
import ru.teterin.tm.enumerated.Status;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static ru.teterin.tm.util.DateUuidParseUtil.toSqlDate;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    public TaskRepository(
        @NotNull final Connection connection
    ) {
        super(connection);
    }

    @Nullable
    private Task fetch(
        @Nullable final ResultSet row
    ) throws Exception {
        if (row == null) {
            return null;
        }
        @NotNull final Task task = new Task();
        task.setId(row.getString(FieldConstant.ID));
        task.setUserId(row.getString(FieldConstant.USER_ID));
        task.setProjectId(row.getString(FieldConstant.PROJECT_ID));
        task.setName(row.getString(FieldConstant.NAME));
        task.setDescription(row.getString(FieldConstant.DESCRIPTION));
        task.setDateCreate(row.getDate(FieldConstant.DATE_CREATE));
        task.setDateStart(row.getDate(FieldConstant.DATE_START));
        task.setDateEnd(row.getDate(FieldConstant.DATE_END));
        task.setStatus(Status.valueOf(row.getString(FieldConstant.STATUS)));
        return task;
    }

    @Nullable
    @Override
    public Task findOne(
        @NotNull final String id
    ) throws Exception {
        @NotNull final String query = "SELECT * FROM app_task WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) {
            resultSet.close();
            statement.close();
            return null;
        }
        @Nullable final Task task = fetch(resultSet);
        resultSet.close();
        statement.close();
        return task;
    }

    @Nullable
    @Override
    public Task findOne(
        @NotNull final String userId,
        @NotNull final String id
    ) throws Exception {
        @NotNull final String query = "SELECT * FROM app_task WHERE id = ? AND userId = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) {
            resultSet.close();
            statement.close();
            return null;
        }
        @Nullable final Task task = fetch(resultSet);
        resultSet.close();
        statement.close();
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAll() throws Exception {
        @NotNull final String query = "SELECT * FROM app_task";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) {
            statement.close();
            return Collections.emptyList();
        }
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) {
            tasks.add(fetch(resultSet));
        }
        statement.close();
        resultSet.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findAll(
        @NotNull final String userId
    ) throws Exception {
        @NotNull final String query = "SELECT * FROM app_task WHERE userId = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) {
            statement.close();
            return Collections.emptyList();
        }
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) {
            tasks.add(fetch(resultSet));
        }
        statement.close();
        resultSet.close();
        return tasks;
    }

    @Override
    public void persist(
        @NotNull final Task task
    ) throws Exception {
        @NotNull final String query = "INSERT INTO app_task VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, task.getId());
        statement.setString(2, task.getUserId());
        statement.setString(3, task.getProjectId());
        statement.setString(4, task.getName());
        statement.setString(5, task.getDescription());
        statement.setDate(6, toSqlDate(task.getDateCreate()));
        statement.setDate(7, toSqlDate(task.getDateStart()));
        statement.setDate(8, toSqlDate(task.getDateEnd()));
        statement.setString(9, task.getStatus().displayName());
        statement.executeUpdate();
        connection.commit();
        statement.close();
    }

    @Override
    public void merge(
        @NotNull final Task task
    ) throws Exception {
        @NotNull final String query = "INSERT INTO app_task VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) " +
            "ON DUPLICATE KEY UPDATE projectId = ?, name = ?, description = ?, dateStart = ?, dateEnd = ?, status = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, task.getId());
        statement.setString(2, task.getUserId());
        statement.setString(3, task.getProjectId());
        statement.setString(4, task.getName());
        statement.setString(5, task.getDescription());
        statement.setDate(6, toSqlDate(task.getDateCreate()));
        statement.setDate(7, toSqlDate(task.getDateStart()));
        statement.setDate(8, toSqlDate(task.getDateEnd()));
        statement.setString(9, task.getStatus().displayName());
        statement.setString(10, task.getProjectId());
        statement.setString(11, task.getName());
        statement.setString(12, task.getDescription());
        statement.setDate(13, toSqlDate(task.getDateStart()));
        statement.setDate(14, toSqlDate(task.getDateEnd()));
        statement.setString(15, task.getStatus().displayName());
        statement.executeUpdate();
        connection.commit();
        statement.close();
    }

    @Override
    public void remove(
        @NotNull final String id
    ) throws Exception {
        @NotNull final String query = "DELETE FROM app_task WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public void remove(
        @NotNull final String userId,
        @NotNull final String id
    ) throws Exception {
        @NotNull final String query = "DELETE FROM app_task WHERE userId =? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String query = "DELETE FROM app_task";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public void removeAll(
        @NotNull final String userId
    ) throws Exception {
        @NotNull final String query = "DELETE FROM app_task WHERE userId = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @NotNull
    @Override
    public Collection<Task> findAllByProjectId(
        @NotNull final String userId,
        @NotNull final String projectId
    ) throws Exception {
        @NotNull final String query = "SELECT * FROM app_task WHERE userId = ? AND projectId = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) {
            statement.close();
            return Collections.emptyList();
        }
        @NotNull final Collection<Task> tasks = new ArrayList<>();
        while (resultSet.next()) {
            tasks.add(fetch(resultSet));
        }
        statement.close();
        resultSet.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> searchByString(
        @NotNull String userId,
        @NotNull String searchString
    ) throws Exception {
        @NotNull final String query = "SELECT * FROM app_task WHERE userId = ? " +
            "AND (name LIKE CONCAT('%',?,'%') OR description LIKE CONCAT('%',?,'%'))";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, searchString);
        statement.setString(3, searchString);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) {
            statement.close();
            return Collections.emptyList();
        }
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) {
            tasks.add(fetch(resultSet));
        }
        statement.close();
        resultSet.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> sortByDateCreate(
        @NotNull final String userId,
        @NotNull final String sortType
    ) throws Exception {
        @NotNull String query = "SELECT * FROM app_task WHERE userId = ? ORDER BY dateCreate";
        if (Constant.DESC.equals(sortType)) {
            query = "SELECT * FROM app_task WHERE userId = ? ORDER BY dateCreate DESC";
        }
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) {
            statement.close();
            return Collections.emptyList();
        }
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) {
            tasks.add(fetch(resultSet));
        }
        statement.close();
        resultSet.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> sortByDateStart(
        @NotNull final String userId,
        @NotNull final String sortType
    ) throws Exception {
        @NotNull String query = "SELECT * FROM app_task WHERE userId = ? ORDER BY dateStart";
        if (Constant.DESC.equals(sortType)) {
            query = "SELECT * FROM app_task WHERE userId = ? ORDER BY dateStart DESC";
        }
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) {
            statement.close();
            return Collections.emptyList();
        }
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) {
            tasks.add(fetch(resultSet));
        }
        statement.close();
        resultSet.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> sortByDateEnd(
        @NotNull final String userId,
        @NotNull final String sortType
    ) throws Exception {
        @NotNull String query = "SELECT * FROM app_task WHERE userId = ? ORDER BY dateEnd";
        if (Constant.DESC.equals(sortType)) {
            query = "SELECT * FROM app_task WHERE userId = ? ORDER BY dateEnd DESC";
        }
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) {
            statement.close();
            return Collections.emptyList();
        }
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) {
            tasks.add(fetch(resultSet));
        }
        statement.close();
        resultSet.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> sortByStatus(
        @NotNull final String userId,
        @NotNull final String sortType
    ) throws Exception {
        @NotNull String query = "SELECT * FROM app_task WHERE userId = ? ORDER BY status";
        if (Constant.DESC.equals(sortType)) {
            query = "SELECT * FROM app_task WHERE userId = ? ORDER BY status DESC";
        }
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) {
            statement.close();
            return Collections.emptyList();
        }
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) {
            tasks.add(fetch(resultSet));
        }
        statement.close();
        resultSet.close();
        return tasks;
    }

}
