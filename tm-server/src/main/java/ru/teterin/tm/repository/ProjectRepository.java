package ru.teterin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.repository.IProjectRepository;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.constant.FieldConstant;
import ru.teterin.tm.entity.Project;
import ru.teterin.tm.enumerated.Status;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static ru.teterin.tm.util.DateUuidParseUtil.toSqlDate;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    public ProjectRepository(
        @NotNull final Connection connection
    ) {
        super(connection);
    }

    @Nullable
    private Project fetch(
        @Nullable final ResultSet row
    ) throws Exception {
        if (row == null) {
            return null;
        }
        @NotNull final Project project = new Project();
        project.setId(row.getString(FieldConstant.ID));
        project.setUserId(row.getString(FieldConstant.USER_ID));
        project.setName(row.getString(FieldConstant.NAME));
        project.setDescription(row.getString(FieldConstant.DESCRIPTION));
        project.setDateCreate(row.getDate(FieldConstant.DATE_CREATE));
        project.setDateStart(row.getDate(FieldConstant.DATE_START));
        project.setDateEnd(row.getDate(FieldConstant.DATE_END));
        project.setStatus(Status.valueOf(row.getString(FieldConstant.STATUS)));
        return project;
    }

    @Nullable
    @Override
    public Project findOne(
        @NotNull final String id
    ) throws Exception {
        @NotNull final String query = "SELECT * FROM app_project WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) {
            statement.close();
            return null;
        }
        @Nullable final Project project = fetch(resultSet);
        resultSet.close();
        statement.close();
        return project;
    }

    @Nullable
    @Override
    public Project findOne(
        @NotNull final String userId,
        @NotNull final String id
    ) throws Exception {
        @NotNull final String query = "SELECT * FROM app_project WHERE id = ? AND userId = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) {
            resultSet.close();
            statement.close();
            return null;
        }
        @Nullable final Project project = fetch(resultSet);
        resultSet.close();
        statement.close();
        return project;
    }

    @NotNull
    @Override
    public List<Project> findAll() throws Exception {
        @NotNull final String query = "SELECT * FROM app_project";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) {
            statement.close();
            return Collections.emptyList();
        }
        @NotNull final List<Project> projects = new ArrayList<>();
        while (resultSet.next()) {
            projects.add(fetch(resultSet));
        }
        statement.close();
        resultSet.close();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findAll(
        @NotNull final String userId
    ) throws Exception {
        @NotNull final String query = "SELECT * FROM app_project WHERE userId = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) {
            statement.close();
            return Collections.emptyList();
        }
        List<Project> projects = new ArrayList<>();
        while (resultSet.next()) {
            projects.add(fetch(resultSet));
        }
        statement.close();
        resultSet.close();
        return projects;
    }

    @Override
    public void persist(
        @NotNull final Project project
    ) throws Exception {
        @NotNull final String query = "INSERT INTO app_project VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, project.getId());
        statement.setString(2, project.getUserId());
        statement.setString(3, project.getName());
        statement.setString(4, project.getDescription());
        statement.setDate(5, toSqlDate(project.getDateCreate()));
        statement.setDate(6, toSqlDate(project.getDateStart()));
        statement.setDate(7, toSqlDate(project.getDateEnd()));
        statement.setString(8, project.getStatus().displayName());
        statement.executeUpdate();
        connection.commit();
        statement.close();
    }

    @Override
    public void merge(
        @NotNull final Project project
    ) throws Exception {
        @NotNull final String query = "INSERT INTO app_project VALUES (?, ?, ?, ?, ?, ?, ?, ?) " +
            "ON DUPLICATE KEY UPDATE name = ?, description = ?, dateStart = ?, dateEnd = ?, status = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, project.getId());
        statement.setString(2, project.getUserId());
        statement.setString(3, project.getName());
        statement.setString(4, project.getDescription());
        statement.setDate(5, toSqlDate(project.getDateCreate()));
        statement.setDate(6, toSqlDate(project.getDateStart()));
        statement.setDate(7, toSqlDate(project.getDateEnd()));
        statement.setString(8, project.getStatus().displayName());
        statement.setString(9, project.getName());
        statement.setString(10, project.getDescription());
        statement.setDate(11, toSqlDate(project.getDateStart()));
        statement.setDate(12, toSqlDate(project.getDateEnd()));
        statement.setString(13, project.getStatus().displayName());
        statement.executeUpdate();
        connection.commit();
        statement.close();
    }

    @Override
    public void remove(
        @NotNull final String id
    ) throws Exception {
        @NotNull final String query = "DELETE FROM app_project WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public void remove(
        @NotNull final String userId,
        @NotNull final String id
    ) throws Exception {
        @NotNull final String query = "DELETE FROM app_project WHERE userId =? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String query = "DELETE FROM app_project";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public void removeAll(
        @NotNull final String userId
    ) throws Exception {
        @NotNull final String query = "DELETE FROM app_project WHERE userId = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @NotNull
    @Override
    public Collection<Project> searchByString(
        @NotNull String userId,
        @NotNull String searchString
    ) throws Exception {
        @NotNull final String query = "SELECT * FROM app_project WHERE userId = ? " +
            "AND (name LIKE CONCAT('%',?,'%') OR description LIKE CONCAT('%',?,'%'))";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, searchString);
        statement.setString(3, searchString);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) {
            statement.close();
            return Collections.emptyList();
        }
        @NotNull final List<Project> projects = new ArrayList<>();
        while (resultSet.next()) {
            projects.add(fetch(resultSet));
        }
        statement.close();
        resultSet.close();
        return projects;
    }

    @NotNull
    @Override
    public Collection<Project> sortByDateCreate(
        @NotNull final String userId,
        @NotNull final String sortType
    ) throws Exception {
        @NotNull String query = "SELECT * FROM app_project WHERE userId = ? ORDER BY dateCreate";
        if (Constant.DESC.equals(sortType)) {
            query = "SELECT * FROM app_project WHERE userId = ? ORDER BY dateCreate DESC";
        }
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) {
            statement.close();
            return Collections.emptyList();
        }
        @NotNull final List<Project> projects = new ArrayList<>();
        while (resultSet.next()) {
            projects.add(fetch(resultSet));
        }
        statement.close();
        resultSet.close();
        return projects;
    }

    @NotNull
    @Override
    public Collection<Project> sortByDateStart(
        @NotNull final String userId,
        @NotNull final String sortType
    ) throws Exception {
        @NotNull String query = "SELECT * FROM app_project WHERE userId = ? ORDER BY dateStart";
        if (Constant.DESC.equals(sortType)) {
            query = "SELECT * FROM app_project WHERE userId = ? ORDER BY dateStart DESC";
        }
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) {
            statement.close();
            return Collections.emptyList();
        }
        @NotNull final List<Project> projects = new ArrayList<>();
        while (resultSet.next()) {
            projects.add(fetch(resultSet));
        }
        statement.close();
        resultSet.close();
        return projects;
    }

    @NotNull
    @Override
    public Collection<Project> sortByDateEnd(
        @NotNull final String userId,
        @NotNull final String sortType
    ) throws Exception {
        @NotNull String query = "SELECT * FROM app_project WHERE userId = ? ORDER BY dateEnd";
        if (Constant.DESC.equals(sortType)) {
            query = "SELECT * FROM app_project WHERE userId = ? ORDER BY dateEnd DESC";
        }
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) {
            statement.close();
            return Collections.emptyList();
        }
        @NotNull final List<Project> projects = new ArrayList<>();
        while (resultSet.next()) {
            projects.add(fetch(resultSet));
        }
        statement.close();
        resultSet.close();
        return projects;
    }

    @NotNull
    @Override
    public Collection<Project> sortByStatus(
        @NotNull final String userId,
        @NotNull final String sortType
    ) throws Exception {
        @NotNull String query = "SELECT * FROM app_project WHERE userId = ? ORDER BY status";
        if (Constant.DESC.equals(sortType)) {
            query = "SELECT * FROM app_project WHERE userId = ? ORDER BY status DESC";
        }
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) {
            statement.close();
            return Collections.emptyList();
        }
        @NotNull final List<Project> projects = new ArrayList<>();
        while (resultSet.next()) {
            projects.add(fetch(resultSet));
        }
        statement.close();
        resultSet.close();
        return projects;
    }

}
