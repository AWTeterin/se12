package ru.teterin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.repository.ISessionRepository;
import ru.teterin.tm.constant.ExceptionConstant;
import ru.teterin.tm.constant.FieldConstant;
import ru.teterin.tm.entity.Session;
import ru.teterin.tm.enumerated.Role;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(
        @NotNull final Connection connection
    ) {
        super(connection);
    }

    @Nullable
    @Override
    public Session findOne(
        @NotNull final String id
    ) throws Exception {
        @NotNull final String query = "SELECT * FROM app_session WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) {
            return null;
        }
        @Nullable final Session session = fetch(resultSet);
        resultSet.close();
        statement.close();
        return session;
    }

    @NotNull
    @Override
    public List<Session> findAll() throws Exception {
        @NotNull final String query = "SELECT * FROM app_session";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) {
            return Collections.emptyList();
        }
        List<Session> sessions = new ArrayList<>();
        while (resultSet.next()) {
            sessions.add(fetch(resultSet));
        }
        statement.close();
        resultSet.close();
        return sessions;
    }

    @Override
    public void persist(
        @NotNull final Session session
    ) throws Exception {
        @NotNull final String query = "INSERT INTO app_session VALUES (?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, session.getId());
        statement.setString(2, session.getUserId());
        statement.setString(3, session.getRole().displayName());
        statement.setLong(4, session.getTimestamp());
        statement.setString(5, session.getSignature());
        statement.executeUpdate();
        connection.commit();
        statement.close();
    }


    @Override
    public void merge(
        @NotNull final Session session
    ) throws Exception {
        @NotNull final String query = "INSERT INTO app_session VALUES (?, ?, ?, ?, ?) " +
            "ON DUPLICATE KEY UPDATE signature = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, session.getId());
        statement.setString(2, session.getUserId());
        statement.setString(3, session.getRole().displayName());
        statement.setLong(4, session.getTimestamp());
        statement.setString(5, session.getSignature());
        statement.setString(6, session.getSignature());
        statement.executeUpdate();
        connection.commit();
        statement.close();
    }

    @Override
    public void remove(
        @NotNull final String id
    ) throws Exception {
        @NotNull final String query = "DELETE FROM app_session WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String query = "DELETE FROM app_session";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Nullable
    private Session fetch(
        @Nullable final ResultSet row
    ) throws Exception {
        if (row == null) {
            return null;
        }
        @NotNull final Session session = new Session();
        session.setId(row.getString(FieldConstant.ID));
        session.setUserId(row.getString(FieldConstant.USER_ID));
        session.setRole(Role.valueOf(row.getString(FieldConstant.ROLE)));
        session.setTimestamp(row.getLong(FieldConstant.TIMESTAMP));
        session.setSignature(row.getString(FieldConstant.SIGNATURE));
        return session;
    }

    @Override
    public void validate(
        @NotNull final String userId,
        @NotNull final String sessionId
    ) throws Exception {
        @Nullable final Session session = findOne(sessionId);
        if (session == null) {
            throw new Exception(ExceptionConstant.NO_SESSION);
        }
        @NotNull final String sessionUserId = session.getUserId();
        final boolean noSession = !userId.equals(sessionUserId);
        if (noSession) {
            throw new Exception(ExceptionConstant.NO_SESSION);
        }
    }

    @Override
    public boolean contains(
        @NotNull final String sessionId
    ) throws Exception {
        @NotNull final String query = "SELECT * FROM app_session WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, sessionId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) {
            return false;
        }
        resultSet.close();
        statement.close();
        return true;
    }

}
