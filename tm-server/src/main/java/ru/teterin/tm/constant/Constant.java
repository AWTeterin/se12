package ru.teterin.tm.constant;

import org.jetbrains.annotations.NotNull;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;

public final class Constant {

    @NotNull
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

    @NotNull
    public static final String PROPERTIES_FILE = "application.properties";

    @NotNull
    public static final Path DIRECTORY = Paths.get(System.getProperty("user.dir"));

    @NotNull
    public static final String BIN_FILE_NAME = "data.bin";

    @NotNull
    public static final String JSON_FILE_NAME = "data.json";

    @NotNull
    public static final String XML_FILE_NAME = "data.xml";

    @NotNull
    public static final String BIN_FILE = DIRECTORY + "\\" + BIN_FILE_NAME;

    @NotNull
    public static final String JSON_FILE = DIRECTORY + "\\" + JSON_FILE_NAME;

    @NotNull
    public static final String XML_FILE = DIRECTORY + "\\" + XML_FILE_NAME;

    @NotNull
    public static final String ASC = "ASC";

    @NotNull
    public static final String DESC = "DESC";

    @NotNull
    public static final String CREATE_ASC_SORT = "11";

    @NotNull
    public static final String CREATE_DESC_SORT = "12";

    @NotNull
    public static final String START_DATE_ASC_SORT = "21";

    @NotNull
    public static final String START_DATE_DESC_SORT = "22";

    @NotNull
    public static final String END_DATE_ASC_SORT = "31";

    @NotNull
    public static final String END_DATE_DESC_SORT = "32";

    @NotNull
    public static final String STATUS_ASC_SORT = "41";

    @NotNull
    public static final String STATUS_DESC_SORT = "42";

}
