package ru.teterin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.endpoint.ITaskEndpoint;
import ru.teterin.tm.api.service.ITaskService;
import ru.teterin.tm.entity.Session;
import ru.teterin.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService(endpointInterface = "ru.teterin.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    private final ITaskService taskService;

    public TaskEndpoint(
        @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        this.taskService = serviceLocator.getTaskService();
    }

    @Override
    @WebMethod
    public void mergeTask(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "task") @Nullable final Task task
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = session.getUserId();
        taskService.merge(userId, task);
    }

    @Override
    @WebMethod
    public void persistTask(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "task") @Nullable final Task task
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = session.getUserId();
        taskService.persist(userId, task);
    }

    @NotNull
    @Override
    @WebMethod
    public Task findOneTask(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "taskId") @Nullable final String taskId
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = session.getUserId();
        @NotNull final Task task = taskService.findOne(userId, taskId);
        return task;
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> findAllTask(
        @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = session.getUserId();
        @NotNull final List<Task> tasks = taskService.findAll(userId);
        return tasks;
    }

    @Override
    @WebMethod
    public void removeTask(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "taskId") @Nullable final String taskId
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = session.getUserId();
        taskService.remove(userId, taskId);
    }

    @Override
    @WebMethod
    public void removeAllTasks(
        @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = session.getUserId();
        taskService.removeAll(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> searchTaskByString(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "string") @Nullable final String string
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = session.getUserId();
        @NotNull final Collection<Task> tasks = taskService.searchByString(userId, string);
        return tasks;
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> sortAllTask(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "sortOption") @Nullable final String sortOption
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = session.getUserId();
        @NotNull final Collection<Task> tasks = taskService.findAndSortAll(userId, sortOption);
        return tasks;
    }

    @Override
    @WebMethod
    public void linkTask(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "projectId") @Nullable final String projectId,
        @WebParam(name = "taskId") @Nullable final String taskId
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = session.getUserId();
        taskService.linkTask(userId, projectId, taskId);
    }

}
