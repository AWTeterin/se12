package ru.teterin.tm.endpoint;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.service.ISessionService;
import ru.teterin.tm.entity.Session;
import ru.teterin.tm.enumerated.Role;

@RequiredArgsConstructor
public abstract class AbstractEndpoint {

    @NotNull
    protected final IServiceLocator serviceLocator;

    public void validateSession(
        @Nullable final Session session
    ) throws Exception {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.validate(session);
    }

    public void validateSessionWithRole(
        @Nullable final Session session,
        @Nullable final Role role
    ) throws Exception {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.validateWithRole(session, role);
    }

}
