package ru.teterin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.endpoint.ISessionEndpoint;
import ru.teterin.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.teterin.tm.api.endpoint.ISessionEndpoint")
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint(
        @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
    }

    @Nullable
    @Override
    @WebMethod
    public Session openSession(
        @WebParam(name = "login") @Nullable final String login,
        @WebParam(name = "password") @Nullable final String password
    ) throws Exception {
        @Nullable final Session session = serviceLocator.getSessionService().open(login, password);
        return session;
    }

    @Nullable
    @Override
    @WebMethod
    public Session closeSession(
        @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        serviceLocator.getSessionService().close(session);
        return session;
    }

}
