package ru.teterin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.endpoint.IDomainEndpoint;
import ru.teterin.tm.api.service.IDomainService;
import ru.teterin.tm.entity.Session;
import ru.teterin.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.teterin.tm.api.endpoint.IDomainEndpoint")
public class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    @NotNull
    private final IDomainService domainService;

    public DomainEndpoint(
        @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        this.domainService = serviceLocator.getDomainService();
    }

    @Override
    @WebMethod
    public void saveBinary(
        @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        validateSessionWithRole(session, Role.ADMIN);
        domainService.saveBinary();
    }

    @Override
    @WebMethod
    public void loadBinary(
        @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        validateSessionWithRole(session, Role.ADMIN);
        domainService.loadBinary();
    }

    @Override
    @WebMethod
    public void saveJsonFasterxml(
        @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        validateSessionWithRole(session, Role.ADMIN);
        domainService.saveJsonFasterxml();
    }

    @Override
    @WebMethod
    public void saveJsonJaxb(
        @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        validateSessionWithRole(session, Role.ADMIN);
        domainService.saveJsonJaxb();
    }

    @Override
    @WebMethod
    public void loadJsonFasterxml(
        @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        validateSessionWithRole(session, Role.ADMIN);
        domainService.loadJsonFasterxml();
    }

    @Override
    @WebMethod
    public void loadJsonJaxb(
        @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        validateSessionWithRole(session, Role.ADMIN);
        domainService.loadJsonJaxb();
    }

    @Override
    @WebMethod
    public void saveXmlFasterxml(
        @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        validateSessionWithRole(session, Role.ADMIN);
        domainService.saveXmlFasterxml();
    }

    @Override
    @WebMethod
    public void saveXmlJaxb(
        @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        validateSessionWithRole(session, Role.ADMIN);
        domainService.saveXmlJaxb();
    }

    @Override
    @WebMethod
    public void loadXmlFasterxml(
        @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        validateSessionWithRole(session, Role.ADMIN);
        domainService.loadXmlFasterxml();
    }

    @Override
    @WebMethod
    public void loadXmlJaxb(
        @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        validateSessionWithRole(session, Role.ADMIN);
        domainService.loadXmlJaxb();
    }

}
