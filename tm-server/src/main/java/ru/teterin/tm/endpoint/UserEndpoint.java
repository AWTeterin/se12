package ru.teterin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.endpoint.IUserEndpoint;
import ru.teterin.tm.api.service.IUserService;
import ru.teterin.tm.entity.Session;
import ru.teterin.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.teterin.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    private final IUserService userService;

    public UserEndpoint(
        @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        this.userService = serviceLocator.getUserService();
    }

    @Override
    @WebMethod
    public void mergeUser(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "user") @Nullable final User user
    ) throws Exception {
        validateSession(session);
        userService.merge(user);
    }

    @Override
    @WebMethod
    public void persistUser(
        @WebParam(name = "user") @Nullable final User user
    ) throws Exception {
        userService.persist(user);
    }

    @NotNull
    @Override
    @WebMethod
    public User findOneUser(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        @Nullable final User user = userService.findOne(userId);
        return user;
    }

    @Override
    @WebMethod
    public void removeUser(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        userService.remove(userId);
    }

    @Override
    @WebMethod
    public boolean loginIsFree(
        @WebParam(name = "login") @Nullable final String login
    ) throws Exception {
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) {
            return true;
        }
        return false;
    }

}
