package ru.teterin.tm.enumerated;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;

@AllArgsConstructor
public enum Status {

    PLANNED("PLANNED"),
    IN_PROGRESS("IN PROGRESS"),
    READY("READY");

    @NotNull
    private final String name;

    @NotNull
    public String displayName() {
        return name;
    }

}
